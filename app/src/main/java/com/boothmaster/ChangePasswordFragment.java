package com.boothmaster;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


public class ChangePasswordFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    Button changePassword;
    EditText rePassword, password;
    private final String METHOD_NAME = "UpdateCustomerPassword";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "UpdateCustomerPassword";
    public ProgressDialog pDialog;
    String password1, rePassword1;
    String mCustomerId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        mActivity = (MainActivity) getActivity();


        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        mCustomerId = pref.getString("id", "");

        Log.d("cust_id", "==" + mCustomerId);

        changePassword = (Button) mRootView.findViewById(R.id.changePassword);

        password = (EditText) mRootView.findViewById(R.id.password);
        rePassword = (EditText) mRootView.findViewById(R.id.RePassword);


        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password1 = password.getText().toString();
                rePassword1 = rePassword.getText().toString();

                if (password1.isEmpty() ) {
                    Toast.makeText(getContext(), "Please enter password.", Toast.LENGTH_SHORT).show();
                    return;
                }else if(password1.length()<3 ){
                    Toast.makeText(getContext(), "Set minimum 3 characters password.", Toast.LENGTH_SHORT).show();
                    return;
                }else if(rePassword1.isEmpty()){
                    Toast.makeText(getContext(), "Please re enter password.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password1.equals(rePassword1)) {
                    showProgressDialog();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                            request.addProperty("Password", password1);
                            request.addProperty("CustomerId", mCustomerId);

                            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                            soapEnvelope.bodyOut = request;
                            soapEnvelope.dotNet = true;
                            soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                            soapEnvelope.setOutputSoapObject(request);

                            HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                            transport.debug = true;
                            transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                            try {
                                transport.call(SOAP_ACTION, soapEnvelope);


                                SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                                Log.d("Result_Login", so.toString());



//                                    Toast.makeText(getActivity(), "Password changed successfully", Toast.LENGTH_SHORT).show();
                                    mActivity.onBackPressed();



                            } catch (XmlPullParserException e) {

                            } catch (Exception e) {
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
//                                    Toast.makeText(getContext(), R.string.no_internet_msg, Toast.LENGTH_SHORT).show();

                                   mActivity.onBackPressed();

                                }
                            });
                        }
                    }).start();
                } else {
                    Toast.makeText(mActivity, "Re-Enter Password not match", Toast.LENGTH_SHORT).show();
                }


            }
        });


        return mRootView;
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Please wait...");
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

}
