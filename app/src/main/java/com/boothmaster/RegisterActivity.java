package com.boothmaster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.model.PolingStationData;
import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends BaseActivity {

    EditText mEditName;
    EditText mEditEmail;
    EditText mEditMobile;
    EditText mEditPassword;
    TextView mTxtChoose3;
    TextView mTxtChoose4;
    int mSelPos3 = -1;
    int mSelPos4 = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        hideSystemUI();
        setContentView(R.layout.activity_register);
        initControl();

    }

    List<PolingStationData> mListPoliing = new ArrayList<>();

    private void initControl() {
        mEditName = findViewById(R.id.edit_name);
        mEditEmail = findViewById(R.id.edit_email);
        mEditMobile = findViewById(R.id.edit_mobile);
        mEditPassword = findViewById(R.id.edit_password);
        mTxtChoose3 = findViewById(R.id.txt_choose3);
        mTxtChoose4 = findViewById(R.id.txt_choose4);
        findViewById(R.id.layout_choose3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu dropDownMenu = new PopupMenu(RegisterActivity.this, v);

                for (int i = 0; i < GlobalConst.SEARCH_PARAMS3[0].length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, GlobalConst.SEARCH_PARAMS3[0][i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        int selPos = menuItem.getItemId() - 1;
                        if (selPos != mSelPos3) {
                            mSelPos4 = -1;
                            mTxtChoose4.setText(R.string.choose_booth);
                        }
                        mSelPos3 = selPos;
                        mTxtChoose3.setText(menuItem.getTitle());
                        loadPolingStation();
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        findViewById(R.id.layout_choose4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelPos3 < 0) {
                    return;
                }
                PopupMenu dropDownMenu = new PopupMenu(RegisterActivity.this, v);

                for (int i = 0; i < mListPoliing.size(); i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, mListPoliing.get(i).polingStationId, Menu.NONE, mListPoliing.get(i).polingStationName);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        mSelPos4 = menuItem.getItemId();
                        mTxtChoose4.setText(menuItem.getTitle());
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        findViewById(R.id.layout_get_started).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
    }

    private final String METHOD_NAME_LOAD_POLING = "LoadPolingStation";
    private final String SOAP_ACTION_LOAD_POLING = GlobalConst.NAMESPACE + "LoadPolingStation";

    private void loadPolingStation() {
        final String name = mTxtChoose3.getText().toString();

        mListPoliing.clear();
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_POLING);
                request.addProperty("ConstituencyName", name);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_POLING, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadPolingStationResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadPolingStationResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                                if (complex.hasProperty("PolingStationId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        PolingStationData data = new PolingStationData();
                                        try {
                                            data.polingStationId = Integer.parseInt(GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationId"));
                                            data.polingStationName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationName");
                                            mListPoliing.add(data);
                                        } catch (Exception e) {

                                        }
                                    }

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            hideProgressDialog();

                                        }
                                    });
                                    return;
                                }
                            } catch (Exception e) {

                            }


                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(RegisterActivity.this, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private final String METHOD_NAME = "RegisterUser";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "RegisterUser";

    private void createAccount() {
        final String name = mEditName.getText().toString();
        final String email = mEditEmail.getText().toString();
        final String mobile = mEditMobile.getText().toString();
        final String password = mEditPassword.getText().toString();
        if (name.equals("") || email.equals("") || mobile.equals("") || password.equals("") || mSelPos4 < 0) {
            Toast.makeText(this, "Please fill all fields.", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("FullName", name);
                request.addProperty("EMail", email);
                request.addProperty("Mobile", mobile);
                request.addProperty("Password", password);
                request.addProperty("ConstituencyName", mTxtChoose3.getText().toString());
                request.addProperty("PolingStationId", String.valueOf(mSelPos4));

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Result_Soap", so.toString());


                    if (so.hasProperty("RegisterUserResult")) {
                        final String result = so.getPropertyAsString("RegisterUserResult");
                        Log.d("Result_Register", result);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (result != null && !result.equals("") && !result.equals("0")) {
                                    try {
                                        if (result.equals("Email already exists.")) {
                                            hideProgressDialog();
                                            Toast.makeText(RegisterActivity.this, result, Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                        int id = Integer.parseInt(result);
                                        SharedPreferences pref = getSharedPreferences(GlobalConst.PREF_NAME, 0);
                                        SharedPreferences.Editor editor = pref.edit();
                                        editor.putString("id", String.valueOf(id));
                                         editor.commit();

                                        signIn(email, password);


                                    } catch (Exception e) {
                                        hideProgressDialog();
                                        Toast.makeText(RegisterActivity.this, result, Toast.LENGTH_SHORT).show();
                                    }

                                } else {
                                    hideProgressDialog();
                                    Toast.makeText(RegisterActivity.this, result, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        return;
                    }

                } catch (XmlPullParserException e) {
                    hideProgressDialog();

                } catch (Exception e) {
                    hideProgressDialog();

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(RegisterActivity.this, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }


    private final String METHOD_NAME_VALIDATE_USER = "ValidateUser";
    private final String SOAP_ACTION_VALIDATE_USER = GlobalConst.NAMESPACE + "ValidateUser";

    private void signIn(final String email, final String password) {
        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Please fill all fields.", Toast.LENGTH_SHORT).show();
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_VALIDATE_USER);
                request.addProperty("UserName", email);
                request.addProperty("Password", password);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_VALIDATE_USER, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Result_Login", so.toString());

                    if (so.hasProperty("ValidateUserResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("ValidateUserResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                                if (complex.hasProperty("CustomerId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        try {
                                            final String customerId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "CustomerId");
                                            final String customerName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "CustomerName");
                                            String mobile = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Mobile");
                                            String email = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Email");
                                            final String roldId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "RoleId");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    hideProgressDialog();
                                                    SharedPreferences pref = getSharedPreferences(GlobalConst.PREF_NAME, 0);
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("id", customerId);
                                                    editor.putString("customerName", customerName);
                                                    try {
                                                        editor.putInt("roleId", Integer.parseInt(roldId));
                                                    } catch (Exception e) {

                                                    }
                                                    editor.commit();
                                                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                            return;
                                        } catch (Exception e) {

                                        }
                                    }


                                }
                            } catch (Exception e) {

                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
                                    Toast.makeText(RegisterActivity.this, R.string.server_fail, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(RegisterActivity.this, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
