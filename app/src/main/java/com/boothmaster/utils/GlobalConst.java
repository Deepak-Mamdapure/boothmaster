package com.boothmaster.utils;

import org.ksoap2.serialization.SoapObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;

public class GlobalConst {

    public static String FULL_URL = "http://booth.psweb.in/api/pollapi.asmx";
    public static String NAMESPACE = "http://tempuri.org/";
    public static String PREF_NAME = "sparentols";


    public final static String[] SEARCH_PARAMS1 = new String[]{
        "S24 - Uttar Pradesh"
    };

    public final static String[][] SEARCH_PARAMS2 = new String[][]{
            {
                  "Siddharth Nagar"
            },
    };

    public final static String[][] SEARCH_PARAMS3 = new String[][]{
            {
                    "302- Shohrathgarh CP",
                    "303- Kapilvastu CP",
                    "304- Bansi CP",
                    "305-Itwa CP",
                    "306- Dumariyaganj CP",
            },
    };

    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String getPropertyAsName(SoapObject soElemt, String key){
        if (soElemt != null && soElemt.hasProperty(key)){
            String str =  soElemt.getPropertyAsString(key);
            if (str.equals("anyType{}")){
                return "";
            }else{
                return str;
            }
        }else{
            return "";
        }
    }

    public static void makeUnique() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i < 11; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i=0; i<3; i++) {
            System.out.println(list.get(i));
        }
    }
}
