package com.boothmaster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class LogoFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    private TextView textViewCustomerName;
    private String customerName;
    private String PolingStationId;
    private TextView textUrl;
    //https://www.youtube.com/watch?v=-N857FgwgiY

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_logo,
                container, false);
        mActivity = (MainActivity) getActivity();


        VideoView mVideoView = (VideoView) mRootView.findViewById(R.id.youtube_player_view);

        String uriPath = "android.resource://com.boothmaster/" + R.raw.booth_master_music;
        Uri uri = Uri.parse(uriPath);
        mVideoView.setVideoURI(uri);
        mVideoView.requestFocus();


        MediaController mediaController = new MediaController(getContext());
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);

        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        customerName = pref.getString("customerName", "");
        initControl();
        setCustomerName();
        getPolingStationId();
        loadData();
        return mRootView;
    }

    private void getPolingStationId() {

        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        PolingStationId = pref.getString("PolingStationId", "");
    }

    private void setCustomerName() {
        textViewCustomerName.setText(customerName);
    }

    private void initControl() {

        textViewCustomerName = (TextView) mRootView.findViewById(R.id.textViewCustomerName);
        textUrl = (TextView) mRootView.findViewById(R.id.textUrl);

        textUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.digitalcorn.com"));
                startActivity(browserIntent);
            }
        });
        mRootView.findViewById(R.id.btn_locate_your_booth).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(1);
            }
        });
        mRootView.findViewById(R.id.btn_upload_your_self).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(2);
            }
        });

        mRootView.findViewById(R.id.btn_information).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(4);
            }
        });
        mRootView.findViewById(R.id.btn_update_amf).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(5);
            }
        });

        if (mActivity.mRoleId == 1) {
            mRootView.findViewById(R.id.btn_update_amf).setVisibility(View.VISIBLE);
        } else {
            mRootView.findViewById(R.id.btn_update_amf).setVisibility(View.GONE);
        }

        mRootView.findViewById(R.id.btn_voter_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWebpage();
            }
        });


        mRootView.findViewById(R.id.btn_view_voting_status).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.setFragment(16);
            }
        });
        if (mActivity.mRoleId == 1) {
            mRootView.findViewById(R.id.btn_view_voting_status).setVisibility(View.GONE);
        } else {
            mRootView.findViewById(R.id.btn_view_voting_status).setVisibility(View.VISIBLE);
        }
    }

    private void openWebpage() {
        String url = "https://electoralsearch.in/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    private final String METHOD_NAME = "LoadHomeScreenData";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadHomeScreenData";

    private void loadData() {

        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("PolingStationId", PolingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadHomeScreenDataResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadHomeScreenDataResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("DocumentElement");
                                SoapObject complex = (SoapObject) soElemt.getProperty("HomeData");

                                if (complex.hasProperty("TotalVoterPercentage")) {


                                    try {
                                        final String totalVoterPercentage = GlobalConst.getPropertyAsName(complex, "TotalVoterPercentage");
                                        final String totalVoterValue = GlobalConst.getPropertyAsName(complex, "TotalVoterValue");
                                        final String totalMalePercentage = GlobalConst.getPropertyAsName(complex, "TotalMalePercentage");
                                        final String totalMaleValue = GlobalConst.getPropertyAsName(complex, "TotalMaleValue");
                                        final String totalFeMalePercentage = GlobalConst.getPropertyAsName(complex, "TotalFeMalePercentage");
                                        final String totalFeMaleValue = GlobalConst.getPropertyAsName(complex, "TotalFeMaleValue");
                                        final String topPerformingBooth = GlobalConst.getPropertyAsName(complex, "TopPerformingBooth");
                                        final String worstPerformingBooth = GlobalConst.getPropertyAsName(complex, "WorstPerformingBooth");
                                        final String lastRecordedTime = GlobalConst.getPropertyAsName(complex, "LastRecordedTime");

                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mActivity.hideProgressDialog();
                                                TextView txtVoterPercentage = mRootView.findViewById(R.id.txt_current_voting);
                                                TextView txtVoterValue1 = mRootView.findViewById(R.id.txt_vote_cnt_1);
                                                TextView txtFeMale = mRootView.findViewById(R.id.txt_female_voter);
                                                TextView txtVoterValue2 = mRootView.findViewById(R.id.txt_vote_cnt_2);
                                                TextView txtrMale = mRootView.findViewById(R.id.txt_male_voter);
                                                TextView txtVoterValue3 = mRootView.findViewById(R.id.txt_vote_cnt_3);
                                                TextView txtTopBooth = mRootView.findViewById(R.id.txt_top_performing_booth);
                                                TextView txtYourBooth = mRootView.findViewById(R.id.txt_your_booth);
                                                TextView txtLastReported = mRootView.findViewById(R.id.txt_last_reported);
                                                txtVoterPercentage.setText(totalVoterPercentage);
                                                txtVoterValue1.setText(totalVoterValue);
                                                txtFeMale.setText(totalFeMalePercentage);
                                                txtVoterValue2.setText(totalFeMaleValue);
                                                txtrMale.setText(totalMalePercentage);
                                                txtVoterValue3.setText(totalMaleValue);
                                                txtTopBooth.setText(topPerformingBooth + ",");
                                                txtYourBooth.setText(worstPerformingBooth);
                                                txtLastReported.setText("(Last Reported: " + lastRecordedTime + ")");
                                            }
                                        });
                                        return;
                                    } catch (Exception e) {

                                    }


                                }
                            } catch (Exception e) {

                            }
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mRootView.findViewById(R.id.txt_no_voting).setVisibility(View.VISIBLE);
                                    mActivity.hideProgressDialog();
                                    Toast.makeText(mActivity, R.string.server_fail, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mRootView.findViewById(R.id.txt_no_voting).setVisibility(View.VISIBLE);
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }
}

