package com.boothmaster;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.utils.GlobalConst;


public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String mCustomerId;
    int mRoleId = 0;
    Boolean isBackPressed = false;

    String mPlanConstituencyName;
    int mPlanPolingStationId;
    private ImageView tv_header_title;
    private NavigationView navigationView;
    private TextView textUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        hideSystemUI();
        setContentView(R.layout.activity_main);


        SharedPreferences pref = getSharedPreferences(GlobalConst.PREF_NAME, 0);
        mCustomerId = pref.getString("id", "");
        mRoleId = pref.getInt("roleId", 0);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_header_title = (ImageView) findViewById(R.id.tv_header_title);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        textUrl = (TextView) findViewById(R.id.textUrl);

        textUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.digitalcorn.com"));
                startActivity(browserIntent);
            }
        });

        setFragment(0);
        checkPermission();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (mPos == -1) {
                setFragment(0);
            } else if (mPos == 1) {
                setFragment(0);
            } else if (mPos == 2) {
                setFragment(0);
            } else if (mPos == 3) {
                super.onBackPressed();
            } else if (mPos == 4) {
                setFragment(0);
            } else if (mPos == 5) {
                setFragment(0);
            } else if (mPos == 11) {
                super.onBackPressed();
            } else if (mPos == 12) {
                setFragment(1);
            } else if (mPos == 13) {
                super.onBackPressed();
            } else if (mPos == 14) {
                setFragment(1);
            } else if (mPos == 16) {
                setFragment(0);
            } else if (mPos == 15) {
                Toast.makeText(MainActivity.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                logoutFromApp();
            } else {
                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_image) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        //Check to see which item was being clicked and perform appropriate action
        switch (item.getItemId()) {
            //Replacing the main content with ContentFragment Which is our Inbox View;
            case R.id.nav_home:
                setFragment(0);
                break;
//            case R.id.nav_photos:
//                setFragment(3);
//                break;
            case R.id.nav_change_password:
                setFragment(15);
                break;
            case R.id.nav_logout:
                logoutFromApp();
                break;

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void logoutFromApp() {
        //Clear shared preference values
        SharedPreferences preferences = getSharedPreferences(GlobalConst.PREF_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        if (mPos != 15) {

            Toast.makeText(MainActivity.this, "Logged out successfully", Toast.LENGTH_LONG).show();
        }

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    int mPos = 0;

    public void setFragment(int pos) {
        mPos = pos;
        if (pos == 0) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new LogoFragment());
            ft.commit();
            setTitle("");
            tv_header_title.setVisibility(View.VISIBLE);

        } else if (pos == 1) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new LocateBoothFragment());
            ft.commit();
            setTitle(R.string.locate_your_booth);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 2) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new UploadImageFragment());
            ft.commit();
            setTitle(R.string.upload_image);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 3) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.fragment, new ViewWhoVotedFragment_New());
            ft.addToBackStack(null);
            ft.commit();
            setTitle(R.string.view_who_voted);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 4) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new InformationFragment());
            ft.commit();
            setTitle(R.string.Information);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 5) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new UpdateVotingStatusFragment());
            ft.commit();
            setTitle(R.string.Update_Voting_Status);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 11) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.fragment, new CommuncationPlanFragment());
            ft.addToBackStack(null);
            ft.commit();
            setTitle(R.string.communication_plan);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 12) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new UploadImageFragment());
            ft.commit();
            setTitle(R.string.upload_image);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 13) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.fragment, new AMFFacilitiesFragment());
            ft.addToBackStack(null);
            ft.commit();
            setTitle(R.string.amf_facilities);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 14) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new GeneralInformationFragment());
            ft.commit();
            setTitle(R.string.general_information);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 15) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new ChangePasswordFragment());
            ft.commit();
            setTitle(R.string.changePassword_information);
            tv_header_title.setVisibility(View.GONE);

        } else if (pos == 16) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new ViewVotingStatusFragment());
            ft.commit();
            setTitle(R.string.view_voting_status);
            tv_header_title.setVisibility(View.GONE);

        }
    }

    static final int REQUEST_PERMISSIONS = 3;

    private void checkPermission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults.length > 0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                    }
                }
            }
        }
    }


}
