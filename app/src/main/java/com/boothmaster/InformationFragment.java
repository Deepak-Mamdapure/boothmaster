package com.boothmaster;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class InformationFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    private TextView textView;
    private WebView wv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_information,
                container, false);
        mActivity = (MainActivity) getActivity();
          wv = (WebView) mRootView.findViewById(R.id.webview);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setAppCacheEnabled(true);
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);


        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        MyWebViewClient webViewClient = new MyWebViewClient();
        wv.setWebViewClient(webViewClient);


        textView = (TextView) mRootView.findViewById(R.id.simpleTextView);
        getInformationPageContent();
        return mRootView;
    }




    private class MyWebViewClient extends WebViewClient {
        @Override
//Implement shouldOverrideUrlLoading//
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

//Check whether the URL contains a whitelisted domain. In this example, we’re checking
//whether the URL contains the “example.com” string//
            if(Uri.parse(url).getHost().endsWith("example.com")) {

//If the URL does contain the “example.com” string, then the shouldOverrideUrlLoading method
//will return ‘false” and the URL will be loaded inside your WebView//
                return false;
            }

//If the URL doesn’t contain this string, then it’ll return “true.” At this point, we’ll
//launch the user’s preferred browser, by firing off an Intent//
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);
            return true;
        }
    }


    private final String METHOD_NAME = "LoadInformationPageContent";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadInformationPageContent";

    private void getInformationPageContent() {

        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Information_Result", "response=" + so.toString());

                    if (so.hasProperty("LoadInformationPageContentResult")) {
                        final String so1 = so.getProperty("LoadInformationPageContentResult").toString();
                        Log.d("Information_Result_Soap", "response=" + so1.toString());

                        setHtmlText(so1);


                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                    e.printStackTrace();
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
//                        Toast.makeText(getActivity(), R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private void setHtmlText(final String so1) {
        // init TextView

        mActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final String mimeType = "text/html";
                final String encoding = "UTF-8";
                wv.getSettings().setLoadsImagesAutomatically(true);
                wv.getSettings().setJavaScriptEnabled(true);
                wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                wv.loadDataWithBaseURL("", so1, mimeType, encoding, "");
                // set Text in TextView using fromHtml() method with version check
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                    textView.setText(Html.fromHtml(so1, Html.FROM_HTML_MODE_LEGACY));
//                } else
//                    textView.setText(Html.fromHtml(so1));

            }
        });

    }

}

