package com.boothmaster;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ViewVotingStatusFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    private ProgressDialog pDialog;
    TextView mTxtChoose3;
    private RadioButton radioButtonGreaterThan;
    private RadioButton radioButtonLessThan;
    private boolean isGreater = true;
    private TextView txt_choose3, txt_select_time, txt_booth_no;
    private Button btn_load_status;
    private String columnSortValue="BoothNo";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_view_voting_status, container, false);
        mActivity = (MainActivity) getActivity();
        txt_choose3 = mRootView.findViewById(R.id.txt_choose3);

        radioButtonGreaterThan = (RadioButton) mRootView.findViewById(R.id.radioButtonGreaterThan);
        radioButtonLessThan = (RadioButton) mRootView.findViewById(R.id.radioButtonLessThan);
        txt_booth_no = (TextView) mRootView.findViewById(R.id.txt_booth_no);
        txt_select_time = (TextView) mRootView.findViewById(R.id.txt_select_time);
        btn_load_status = (Button) mRootView.findViewById(R.id.btn_load_status);

        btn_load_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadVotingStatus();
            }
        });
        radioButtonGreaterThan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isGreater = false;
                }

            }
        });


        radioButtonLessThan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isGreater = true;
                }
            }
        });

        mRootView.findViewById(R.id.layout_choose3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < GlobalConst.SEARCH_PARAMS3[0].length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, GlobalConst.SEARCH_PARAMS3[0][i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        txt_choose3.setText(menuItem.getTitle());
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });


        mRootView.findViewById(R.id.layout_select_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] times = new String[]{
                        "6 am", "7 am", "8 am", "9 am", "10 am", "11 am", "12 pm",
                        "1 pm", "2 pm", "3 pm", "4 pm", "5 pm", "6 pm", "7 pm", "8 pm"
                };
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < times.length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, times[i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        txt_select_time.setText(menuItem.getTitle());
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });


        mRootView.findViewById(R.id.layout_booth_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] times = new String[]{
                        "BoothNo", "PolingStationName", "TotalVoted", "MaleVoted", "FemaleVoted"};
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < times.length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, times[i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        txt_booth_no.setText(menuItem.getTitle());
                        columnSortValue = times[menuItem.getItemId()-1];
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });


        return mRootView;
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Please wait...");
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }


    private final String METHOD_NAME_LoadVAFData = "LoadVotingStatus";
    private final String SOAP_ACTION_LoadVAFData = GlobalConst.NAMESPACE + "LoadVotingStatus";

    private void loadVotingStatus() {

        if (txt_choose3.getText().toString().equals("Select AC")) {
            Toast.makeText(getActivity(), "Please select AC", Toast.LENGTH_SHORT).show();

        } else if (txt_select_time.getText().equals("Select Time")) {
            Toast.makeText(getActivity(), "Please select Time", Toast.LENGTH_SHORT).show();

        } else {

            SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
            final String customerId = pref.getString("id", "");

            showProgressDialog();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LoadVAFData);
                    request.addProperty("CustomerId", customerId);
                    request.addProperty("ConstituencyName", txt_choose3.getText().toString().trim());
                    request.addProperty("StrTime", txt_select_time.getText().toString().trim());
                    request.addProperty("ColumnSort", columnSortValue);
                    request.addProperty("Ascending", isGreater);

                    SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    soapEnvelope.bodyOut = request;
                    soapEnvelope.dotNet = true;
                    soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                    soapEnvelope.setOutputSoapObject(request);

                    HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                    transport.debug = true;
                    transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                    try {
                        transport.call(SOAP_ACTION_LoadVAFData, soapEnvelope);


                        SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                        Log.d("LoadVotingStatus", so.toString());


                        if (so.hasProperty("LoadVotingStatusResult")) {
                            SoapObject so1 = (SoapObject) so.getProperty("LoadVotingStatusResult");

                            if (so1.hasProperty("diffgram")) {
                                hideProgressDialog();

                                try {
                                    SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                    SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                    SoapObject complex = (SoapObject) soElemt.getProperty("Table");


                                    final List<HashMap<String, String>> mListVAFData = new ArrayList<>();


                                    if (complex.hasProperty("Booth")) {

                                        for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                            HashMap<String, String> data = new HashMap<>();

                                            try {
                                                data.put("Booth", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Booth"));
                                                data.put("TotalVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "TotalVoted"));
                                                data.put("MaleVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "MaleVoted"));
                                                data.put("FemaleVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "FeMaleVoted"));
                                                mListVAFData.add(data);

                                            } catch (Exception e) {

                                            }
                                        }


                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                init(mListVAFData);
                                            }
                                        });

                                    }
                                } catch (Exception e) {

                                }

                                return;

                            }

                        }

                    } catch (XmlPullParserException e) {
                        hideProgressDialog();
                    } catch (Exception e) {
                        hideProgressDialog();
                    }

                }
            }).start();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This function add the headers to the table
     **/
    public void addHeaders() {
        TableLayout tl = mRootView.findViewById(R.id.table_main);
        TableRow tr = new TableRow(getActivity());
        tl.removeAllViews();
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Booth", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Total Voted", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Male", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Female", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tl.addView(tr, getTblLayoutParams());
    }

    /**
     * This function add the data to the table
     *
     * @param mListPoliing
     */
    public void addData(List<HashMap<String, String>> mListPoliing) {
        TableLayout tl = mRootView.findViewById(R.id.table_main);

        for (int i = 0; i < mListPoliing.size(); i++) {
            TableRow tr = new TableRow(getActivity());
            tr.setLayoutParams(getLayoutParams());
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("Booth"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("TotalVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("MaleVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("FemaleVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
//            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("TimeUpdated"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tl.addView(tr, getTblLayoutParams());
        }
    }

    public void init(List<HashMap<String, String>> mListPoliing) {
        addHeaders();
        addData(mListPoliing);

    }

    private TextView getTextView(int id, String title, int color, int typeface, int bgColor) {
        if (title == null || title.equals("")) {
            title = "0";
        }
        TextView tv = new TextView(getActivity());
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(color);
        tv.setPadding(10, 10, 10, 10);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }

    @NonNull
    private TableRow.LayoutParams getLayoutParams() {
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }

    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
    }
}
