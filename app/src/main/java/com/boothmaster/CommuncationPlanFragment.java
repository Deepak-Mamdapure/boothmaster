package com.boothmaster;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommuncationPlanFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_communicatio_plan,
                container, false);
        mActivity = (MainActivity) getActivity();


        loadData();

        return mRootView;
    }


    private final String METHOD_NAME = "LoadCommunicationPlan";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadCommunicationPlan";

    private void loadData() {

        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("PolingStationId", mActivity.mPlanPolingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadCommunicationPlanResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadCommunicationPlanResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");


                                if (complex.hasProperty("PolingStationId")) {


                                    try {
                                        final String polingName = GlobalConst.getPropertyAsName(complex, "PolingStationName");
                                        final String bloName = GlobalConst.getPropertyAsName(complex, "BLOName");
                                        final String bloMobileNo = GlobalConst.getPropertyAsName(complex, "BLOMobileNO");

                                        final String commName = GlobalConst.getPropertyAsName(complex, "CommunicationPersonName");
                                        final String commTele = GlobalConst.getPropertyAsName(complex, "CommunicationPersonTelephone");
                                        final String commMobile = GlobalConst.getPropertyAsName(complex, "CommunicationPersonMobile");
                                        final String commWireless = GlobalConst.getPropertyAsName(complex, "CommunicationPersonWireless");

                                        final String policeName = GlobalConst.getPropertyAsName(complex, "PoliceStation");
                                        final String policeDistance = GlobalConst.getPropertyAsName(complex, "PoliceStationDistance");
                                        final String policeLandLine = GlobalConst.getPropertyAsName(complex, "PoliceStationLandLine");
                                        final String policeMobile = GlobalConst.getPropertyAsName(complex, "PoliceStationMobile");

                                        final String zoneNumber = GlobalConst.getPropertyAsName(complex, "ZoneNumber");
                                        final String zoneName = GlobalConst.getPropertyAsName(complex, "ZonalMagistrateName");
                                        final String zonePhone = GlobalConst.getPropertyAsName(complex, "ZonalMagistratePhone");

                                        final String sectorNo = GlobalConst.getPropertyAsName(complex, "SectorNo");
                                        final String sectorName = GlobalConst.getPropertyAsName(complex, "SectorMagistrateName");
                                        final String sectorPhone = GlobalConst.getPropertyAsName(complex, "SectorMagistrateContactNumber");

                                        final String personName1 = GlobalConst.getPropertyAsName(complex, "PersonRunnerName1");
                                        final String personMobile1 = GlobalConst.getPropertyAsName(complex, "PersonRunnerContact1");
                                        final String personName2 = GlobalConst.getPropertyAsName(complex, "PersonRunnerName2");
                                        final String personMobile2 = GlobalConst.getPropertyAsName(complex, "PersonRunnerContact2");

                                        loadVAFData();

                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
//                                                mActivity.hideProgressDialog();

                                                TextView txt_1_1 = mRootView.findViewById(R.id.txt_value_1_1);
                                                TextView txt_1_2 = mRootView.findViewById(R.id.txt_value_1_2);
                                                TextView txt_1_3 = mRootView.findViewById(R.id.txt_value_1_3);

                                                TextView txt_2_1 = mRootView.findViewById(R.id.txt_value_2_1);
                                                TextView txt_2_2 = mRootView.findViewById(R.id.txt_value_2_2);
                                                TextView txt_2_3 = mRootView.findViewById(R.id.txt_value_2_3);
                                                TextView txt_2_4 = mRootView.findViewById(R.id.txt_value_2_4);

                                                TextView txt_3_1 = mRootView.findViewById(R.id.txt_value_3_1);
                                                TextView txt_3_2 = mRootView.findViewById(R.id.txt_value_3_2);
                                                TextView txt_3_3 = mRootView.findViewById(R.id.txt_value_3_3);
                                                TextView txt_3_4 = mRootView.findViewById(R.id.txt_value_3_4);

                                                TextView txt_4_1 = mRootView.findViewById(R.id.txt_value_4_1);
                                                TextView txt_4_2 = mRootView.findViewById(R.id.txt_value_4_2);
                                                TextView txt_4_3 = mRootView.findViewById(R.id.txt_value_4_3);

                                                TextView txt_5_1 = mRootView.findViewById(R.id.txt_value_5_1);
                                                TextView txt_5_2 = mRootView.findViewById(R.id.txt_value_5_2);
                                                TextView txt_5_3 = mRootView.findViewById(R.id.txt_value_5_3);

                                                TextView txt_6_1 = mRootView.findViewById(R.id.txt_value_6_1);
                                                TextView txt_6_2 = mRootView.findViewById(R.id.txt_value_6_2);
                                                TextView txt_6_3 = mRootView.findViewById(R.id.txt_value_6_3);
                                                TextView txt_6_4 = mRootView.findViewById(R.id.txt_value_6_4);

                                                txt_1_1.setText(polingName);
                                                txt_1_2.setText(bloName);
                                                txt_1_3.setText(bloMobileNo);

                                                txt_2_1.setText(commName);
                                                txt_2_2.setText(commTele);
                                                txt_2_3.setText(commMobile);
                                                txt_2_4.setText(commWireless);

                                                txt_3_1.setText(policeName);
                                                txt_3_2.setText(policeDistance);
                                                txt_3_3.setText(policeLandLine);
                                                txt_3_4.setText(policeMobile);

                                                txt_4_1.setText(zoneNumber);
                                                txt_4_2.setText(zoneName);
                                                txt_4_3.setText(zonePhone);

                                                txt_5_1.setText(sectorNo);
                                                txt_5_2.setText(sectorName);
                                                txt_5_3.setText(sectorPhone);

                                                txt_6_1.setText(personName1);
                                                txt_6_2.setText(personMobile1);
                                                txt_6_3.setText(personName2);
                                                txt_6_4.setText(personMobile2);


                                            }
                                        });
                                        return;
                                    } catch (Exception e) {

                                    }


                                }
                            } catch (Exception e) {

                            }
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mRootView.findViewById(R.id.txt_no_voting).setVisibility(View.VISIBLE);
                                    mActivity.hideProgressDialog();
//                                    Toast.makeText(mActivity, R.string.server_fail, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mRootView.findViewById(R.id.txt_no_voting).setVisibility(View.VISIBLE);
                        mActivity.hideProgressDialog();
//                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }


    private final String METHOD_NAME_LoadVAFData = "LoadVAFData";
    private final String SOAP_ACTION_LoadVAFData = GlobalConst.NAMESPACE + "LoadVAFData";

    private void loadVAFData() {

//        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LoadVAFData);
                request.addProperty("PolingStationId", mActivity.mPlanPolingStationId);
//                request.addProperty("PolingStationId", "1703");

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LoadVAFData, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    mActivity.hideProgressDialog();

                    if (so.hasProperty("LoadVAFDataResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadVAFDataResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");


                                final List<HashMap<String, String>> mListVAFData = new ArrayList<>();


                                if (complex.hasProperty("VAFName")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        HashMap<String, String> data = new HashMap<>();

                                        try {
                                            data.put("VAFName", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "VAFName"));
                                            data.put("VAFPhone", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "VAFPhone"));
                                            mListVAFData.add(data);

                                        } catch (Exception e) {

                                        }
                                    }


                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mRootView.findViewById(R.id.layout_VAF).setVisibility(View.VISIBLE);
                                            setVAFData(mListVAFData);
                                        }
                                    });

                                }
                            } catch (Exception e) {

                            }
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mActivity.hideProgressDialog();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {
                    mActivity.hideProgressDialog();
                } catch (Exception e) {
                    mActivity.hideProgressDialog();
                }

//                mActivity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mRootView.findViewById(R.id.txt_no_voting).setVisibility(View.VISIBLE);
//                        mActivity.hideProgressDialog();
//                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        }).start();
    }

    private void setVAFData(List<HashMap<String, String>> mListVAFData) {
        LinearLayout layout_VAF_data = mRootView.findViewById(R.id.layout_VAF_data);
        for (int i = 0; i < mListVAFData.size(); i++) {
//            layout_VAF_data.addView(getTextView("<font color='#4e4b46' size='16'>" + "<b>" + "VAF Name "  + (i+1)  + " : </b>" + mListVAFData.get(i).get("VAFName"), 0));
            String text = "VAF Name "  + (i+1)  + ":" ;
            String text2 = mListVAFData.get(i).get("VAFName");
            String styledText = "<b><font color='#4e4b46' size='24'> " +text+" </font></b>."+text2;


            layout_VAF_data.addView(getTextView(styledText,0));
            layout_VAF_data.addView(getTextView("<font color='#4e4b46' size='16'>" + "<b>" + "VAF Phone "  + (i+1)  + " :</b> " + mListVAFData.get(i).get("VAFPhone"), 15));
        }

    }

    private TextView getTextView(String title, int margin) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView tv = new TextView(getActivity());
        tv.setText(Html.fromHtml(title));
        tv.setPadding(10, 10, 10, 0);
        tv.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
        params.setMargins(0, 0, 0, margin);
        tv.setLayoutParams(params);
        return tv;
    }

    @Override
    public void onStop() {
        super.onStop();
        mActivity.mPos = -1;
    }
}

