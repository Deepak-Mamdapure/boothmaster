package com.boothmaster;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boothmaster.model.ImageData;
import com.boothmaster.utils.GlobalConst;

import com.squareup.picasso.Picasso;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;

public class ViewWhoVotedFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    List<ImageData> mListData = new ArrayList<>();
    public ProgressDialog pDialog;
    private final String METHOD_NAME = "LoadVoterPhotoForPolingStationId";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadVoterPhotoForPolingStationId";
    List<ImageData> mList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_view_who_voted,
                container, false);
        mActivity = (MainActivity) getActivity();


//        initControl();
        callVoterPhotosAPI();

        return mRootView;
    }

    private void initControl() {
        for (int i = 0; i < 20; i++) {
            ImageData data = new ImageData();
            data.url = "https://www.uelc.ca/wp-content/uploads/2018/05/how-a-blepharoplasty-procedure-changes-your-face-1.jpg";
            data.text = "name " + (i + 1);
            mListData.add(data);
            data = new ImageData();
            data.url = "https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
            data.text = "name " + (i + 1);
            mListData.add(data);
            data = new ImageData();
            data.url = "https://boygeniusreport.files.wordpress.com/2019/02/download-cropped.jpg?quality=98&strip=all&w=782";
            data.text = "name " + (i + 1);
            mListData.add(data);
            data = new ImageData();
            data.url = "http://images.summitmedia-digital.com/cosmo/images/2017/11/10/5-best-things-about-having-a-round-face-main2-1510299634.jpg";
            data.text = "name " + (i + 1);
            mListData.add(data);
            data = new ImageData();
            data.url = "https://vg-images.condecdn.net/image/56ozjwRpLgg/crop/405/f/original-7.jpg";
            data.text = "name " + (i + 1);
            mListData.add(data);
        }

        ViewPager pager = mRootView.findViewById(R.id.view_pager);
        PagerAdapter adapter = new ImagePagerAdapter(mActivity, mListData);
        pager.setAdapter(adapter);

        TabLayout tabLayout = mRootView.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pager, true);
    }

    public class ImagePagerAdapter extends PagerAdapter {

        Context mContext;
        List<ImageData> mList;

        public ImagePagerAdapter(Context context, List<ImageData> list) {
            mContext = context;
            mList = list;
        }

        @Override
        public int getCount() {
            if (mList.size() == 0) {
                return 0;
            } else {
                return ((mList.size() - 1) / 9 + 1);
            }
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, int position) {

            LayoutInflater inflater = LayoutInflater.from(mContext);
            View contentView = inflater.inflate(R.layout.grid_screen, null);
            for (int i = 0; i < 9; i++) {
                int pos = position * 9 + i;
                int imgId = GlobalConst.getResId("img_" + (i + 1), R.id.class);
                int txtId = GlobalConst.getResId("txt_" + (i + 1), R.id.class);
                ImageView imgView = (ImageView) contentView.findViewById(imgId);
                TextView txtView = (TextView) contentView.findViewById(txtId);
                if (pos < mListData.size()) {
                    imgView.setVisibility(View.VISIBLE);
                    txtView.setVisibility(View.VISIBLE);
                    ImageData data = mListData.get(pos);
                    Picasso.with(mContext).load(data.url).fit().centerCrop().into(imgView);
                    txtView.setText(data.text);
                } else {
                    imgView.setVisibility(View.INVISIBLE);
                    txtView.setVisibility(View.INVISIBLE);

                }
            }
            container.addView(contentView);
            return contentView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mActivity.mPos = -1;
    }


    public void callVoterPhotosAPI() {
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        final String PolingStationId = pref.getString("PolingStationId", "");
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("PolingStationId", PolingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);
                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Load_Photo", "response=" + so.toString());


                    if (so.hasProperty("LoadVoterPhotoForPolingStationIdResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadVoterPhotoForPolingStationIdResult");

                        if (so1.hasProperty("diffgram")) {

                            SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                            SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                            SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                            if (complex.hasProperty("CustomerName")) {

                                try {


                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {

                                        try {
                                            ImageData data = new ImageData();
                                            data.text = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i),"CustomerName");
                                            data.url = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i),"VotingPhoto").trim();

                                            mListData.add(data);


                                        } catch (Exception e) {

                                        }
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ViewPager pager = mRootView.findViewById(R.id.view_pager);
                                            PagerAdapter adapter = new ImagePagerAdapter(mActivity, mListData);
                                            pager.setAdapter(adapter);

                                            TabLayout tabLayout = mRootView.findViewById(R.id.tab_layout);
                                            tabLayout.setupWithViewPager(pager, true);
                                            hideProgressDialog();
                                        }
                                    });

                                    hideProgressDialog();


                                } catch (Exception e) {

                                }


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideProgressDialog();
                                    }
                                });
                                return;

                            }
                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }


            }
        }).start();
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
