package com.boothmaster;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.model.PolingStationData;
import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;

public class LocateBoothFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    //    TextView mTxtChoose1;
//    TextView mTxtChoose2;
    TextView mTxtChoose3;
    TextView mTxtChoose4;
    //    int mSelPos1 = 0;
//    int mSelPos2 = 0;
    int mSelPos3 = -1;
    int mSelPos4 = -1;
    private TextView textViewBoothName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_locate_your_booth,
                container, false);
        mActivity = (MainActivity) getActivity();
        initControl();
        textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar");

        if (mActivity.mRoleId == 1) {
            getConstituencyName();
            getPolingStationName();
            textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar >  " + mTxtChoose3.getText().toString().trim() + " > " + mTxtChoose4.getText().toString().trim());
            setUIVisibility(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE);

        } else if (mActivity.mRoleId == 2 || mActivity.mRoleId == 3 || mActivity.mRoleId == 4) {
            getConstituencyName();
            textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar > " + mTxtChoose3.getText().toString().trim());
            loadPolingStation();
            setUIVisibility(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.VISIBLE);

        } else {
            setUIVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);

        }
        return mRootView;
    }

    private void getConstituencyName() {
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        String constituencyName = pref.getString("ConstituencyName", "");
        String ConstituencyId = pref.getString("ConstituencyId", "");
        mTxtChoose3.setText(constituencyName);
        mSelPos3 = Integer.parseInt(ConstituencyId);

    }

    private void getPolingStationName() {
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        String PolingStationName = pref.getString("PolingStationName", "");
        String PolingStationId = pref.getString("PolingStationId", "");
        mTxtChoose4.setText(PolingStationName);
        mSelPos4 = Integer.parseInt(PolingStationId);

    }

    private void setUIVisibility(int allText, int State, int area, int Constituency, int booth) {
        textViewBoothName.setVisibility(allText);
        mRootView.findViewById(R.id.layout_choose3).setVisibility(Constituency);
        mRootView.findViewById(R.id.layout_choose4).setVisibility(booth);
    }

    private void initControl() {

        textViewBoothName = (TextView) mRootView.findViewById(R.id.textViewBoothName);

        mRootView.findViewById(R.id.btn_communication_plan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mSelPos4 < 0) {
                    Toast.makeText(mActivity, "Please choose Booth", Toast.LENGTH_SHORT).show();
                    return;
                }
                mActivity.mPlanPolingStationId = mSelPos4;
                mActivity.setFragment(11);
            }
        });
        mRootView.findViewById(R.id.btn_amf_facilities).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelPos4 < 0) {
                    Toast.makeText(mActivity, "Please choose Booth", Toast.LENGTH_SHORT).show();
                    return;
                }
                mActivity.mPlanPolingStationId = mSelPos4;
                mActivity.mPlanConstituencyName = mTxtChoose3.getText().toString();
                mActivity.setFragment(13);
            }
        });


        mRootView.findViewById(R.id.btn_amf_facilities).setVisibility(View.VISIBLE);

        mRootView.findViewById(R.id.btn_general_information).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelPos4 < 0) {
                    Toast.makeText(mActivity, "Please choose Booth", Toast.LENGTH_SHORT).show();
                    return;
                }
                mActivity.mPlanPolingStationId = mSelPos4;
                mActivity.mPlanConstituencyName = mTxtChoose3.getText().toString();
                mActivity.setFragment(14);
            }
        });
        if (mActivity.mRoleId > 0) {
            mRootView.findViewById(R.id.btn_general_information).setVisibility(View.VISIBLE);
        } else {
            mRootView.findViewById(R.id.btn_general_information).setVisibility(View.GONE);
        }
        mRootView.findViewById(R.id.btn_view_who_voted).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelPos4 < 0) {
                    Toast.makeText(mActivity, "Please choose Booth", Toast.LENGTH_SHORT).show();
                    return;
                }
                mActivity.setFragment(3);
            }
        });

        mTxtChoose3 = mRootView.findViewById(R.id.txt_choose3);
        mTxtChoose4 = mRootView.findViewById(R.id.txt_choose4);
        mRootView.findViewById(R.id.layout_choose3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < GlobalConst.SEARCH_PARAMS3[0].length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, GlobalConst.SEARCH_PARAMS3[0][i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        int selPos = menuItem.getItemId() - 1;
                        if (selPos != mSelPos3) {
                            mSelPos4 = -1;
                            mTxtChoose4.setText(R.string.choose_booth);
                        }
                        mSelPos3 = selPos;
                        mTxtChoose3.setText(menuItem.getTitle());
                        loadPolingStation();
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        mRootView.findViewById(R.id.layout_choose4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mActivity.mRoleId != 2 && mActivity.mRoleId != 3 && mActivity.mRoleId != 4) {
                    if (mSelPos3 < 0) {
                        return;
                    }
                }
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < mListPoliing.size(); i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, mListPoliing.get(i).polingStationId, Menu.NONE, mListPoliing.get(i).polingStationName);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        mSelPos4 = menuItem.getItemId();
                        mTxtChoose4.setText(menuItem.getTitle());
                        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("PolingStationId", String.valueOf(mSelPos4));
                        editor.commit();
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
    }


    List<PolingStationData> mListPoliing = new ArrayList<>();

    private final String METHOD_NAME_LOAD_POLING = "LoadPolingStationForUser";
    private final String SOAP_ACTION_LOAD_POLING = GlobalConst.NAMESPACE + "LoadPolingStationForUser";

    private void loadPolingStation() {
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        final String customerId = pref.getString("id", "");

        final String name = mTxtChoose3.getText().toString().trim();

        mListPoliing.clear();
        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_POLING);
                request.addProperty("ConstituencyName", name);
                request.addProperty("CustomerId", customerId);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_POLING, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("LoadPolingStationFor", so.toString());
                    if (so.hasProperty("LoadPolingStationForUserResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadPolingStationForUserResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("MyData");

                                if (complex.hasProperty("PolingStationId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        PolingStationData data = new PolingStationData();
                                        try {
                                            data.polingStationId = Integer.parseInt(GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationId"));
                                            data.polingStationName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationName");
                                            mListPoliing.add(data);
                                        } catch (Exception e) {

                                        }
                                    }

                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mActivity.hideProgressDialog();

                                        }
                                    });
                                    return;
                                }
                            } catch (Exception e) {

                            }


                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }
}
