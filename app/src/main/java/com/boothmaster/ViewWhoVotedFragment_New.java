package com.boothmaster;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.boothmaster.load_images.CarRecyclerViewDataAdapter;
import com.boothmaster.load_images.CarRecyclerViewItem;
import com.boothmaster.model.ImageData;
import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;

public class ViewWhoVotedFragment_New extends Fragment {
    View mRootView;
    MainActivity mActivity;
    List<ImageData> mListData = new ArrayList<>();
    public ProgressDialog pDialog;
    private final String METHOD_NAME = "LoadVoterPhotoForPolingStationId";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadVoterPhotoForPolingStationId";

    private List<CarRecyclerViewItem> carItemList = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_view_who_voted_new,
                container, false);
        mActivity = (MainActivity) getActivity();

        callVoterPhotosAPI();
        return mRootView;
    }

    private void initializeCarItemList(List<ImageData> mListData) {

        carItemList = new ArrayList<CarRecyclerViewItem>();
        for (int i = 0; i < mListData.size(); i++) {
            carItemList.add(new CarRecyclerViewItem(mListData.get(i).text, mListData.get(i).url));
        }
        initialiseImageAdapter();


    }

    private void initialiseImageAdapter() {
        // Create the recyclerview.
        RecyclerView carRecyclerView = (RecyclerView) mRootView.findViewById(R.id.card_view_recycler_list);
        // Create the grid layout manager with 2 columns.
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        // Set layout manager.
        carRecyclerView.setLayoutManager(gridLayoutManager);

        // Create car recycler view data adapter with car item list.
        CarRecyclerViewDataAdapter carDataAdapter = new CarRecyclerViewDataAdapter(getActivity(), carItemList);
        // Set data adapter.
        carRecyclerView.setAdapter(carDataAdapter);
    }

    @Override
    public void onStop() {
        super.onStop();
        mActivity.mPos = -1;
    }


    public void callVoterPhotosAPI() {
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        final String PolingStationId = pref.getString("PolingStationId", "");
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("PolingStationId", PolingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);
                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Load_Photo", "response=" + so.toString());


                    if (so.hasProperty("LoadVoterPhotoForPolingStationIdResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadVoterPhotoForPolingStationIdResult");

                        if (so1.hasProperty("diffgram")) {

                            SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                            SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                            SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                            if (complex.hasProperty("CustomerName")) {

                                try {


                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {

                                        try {
                                            ImageData data = new ImageData();
                                            data.text = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "CustomerName");
                                            data.url = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "VotingPhoto").trim();

                                            mListData.add(data);


                                        } catch (Exception e) {

                                        }
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            initializeCarItemList(mListData);
                                            hideProgressDialog();
                                        }
                                    });

                                    hideProgressDialog();


                                } catch (Exception e) {
                                    hideProgressDialog();

                                }


                                return;

                            } else {
                                hideProgressDialog();
                            }
                        }

                    } else {
                        hideProgressDialog();
                    }

                } catch (XmlPullParserException e) {
                    hideProgressDialog();

                } catch (Exception e) {
                    hideProgressDialog();

                }


            }
        }).start();
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please wait...");
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
