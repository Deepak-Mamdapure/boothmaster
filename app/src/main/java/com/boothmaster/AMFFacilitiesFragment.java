package com.boothmaster;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.model.AmfFacilitiesData;
import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.List;

public class AMFFacilitiesFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    LinearLayout mLayoutContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_amf_facilities,
                container, false);
        mActivity = (MainActivity)getActivity();

        initControl();
        loadData();
        return mRootView;
    }

    private void initControl(){
        mLayoutContainer = mRootView.findViewById(R.id.layout_container);
        if (mActivity.mRoleId == 2){
            mRootView.findViewById(R.id.btn_submit).setVisibility(View.VISIBLE);
            mRootView.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submit();
                }
            });
        }else{
            mRootView.findViewById(R.id.btn_submit).setVisibility(View.GONE);
        }

    }

    List<AmfFacilitiesData> mListData = new ArrayList<>();

    private final String METHOD_NAME_LOAD_POLING = "LoadAMFForPolingStationId";
    private final String SOAP_ACTION_LOAD_POLING = GlobalConst.NAMESPACE + "LoadAMFForPolingStationId";
    private void loadData() {
        mActivity.showProgressDialog();
        mListData.clear();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_POLING);
                request.addProperty("PolingStationId", mActivity.mPlanPolingStationId);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_POLING, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadAMFForPolingStationIdResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadAMFForPolingStationIdResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                                if (complex.hasProperty("AmfFacilitieId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        AmfFacilitiesData data = new AmfFacilitiesData();
                                        try {
                                            data.id = Integer.parseInt(GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "AmfFacilitieId"));
                                            data.available = Integer.parseInt(GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Available"));
                                            data.name = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "AmfFacilitieName");
                                            mListData.add(data);
                                        }catch (Exception e){

                                        }
                                    }

                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                         mActivity.hideProgressDialog();
                                         LayoutInflater inflater = LayoutInflater.from(mActivity);
                                         for (int i= 0; i<mListData.size(); i++){
                                             AmfFacilitiesData data = mListData.get(i);
                                             View view = inflater.inflate(R.layout.row_amf_facilities, null);
                                             TextView txtLabel = view.findViewById(R.id.txt_label);
                                             TextView txtValue = view.findViewById(R.id.txt_value);
                                             CheckBox chkValue = view.findViewById(R.id.chk_value);
                                             txtLabel.setText(data.name);
                                             if (data.available > 0) {
                                                 txtValue.setText("हाँ");
                                                 chkValue.setChecked(true);
                                             }else{
                                                 txtValue.setText("नहीं");
                                                 chkValue.setChecked(false);
                                             }
                                             if (mActivity.mRoleId == 2){
                                                 txtValue.setVisibility(View.GONE);
                                                 chkValue.setVisibility(View.VISIBLE);
                                                 chkValue.setTag(data.id);
                                             }else{
                                                 txtValue.setVisibility(View.VISIBLE);
                                                 chkValue.setVisibility(View.GONE);
                                             }
                                             mLayoutContainer.addView(view);
                                         }

                                        }
                                    });
                                    return;
                                }
                            }catch (Exception e){

                            }


                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private final String METHOD_NAME_SUBMIT = "UpdateAMFFacilityStatus";
    private final String SOAP_ACTION_SUBMIT = GlobalConst.NAMESPACE + "UpdateAMFFacilityStatus";
    String mIds;
    private void submit() {
        mIds = "";
        for (int i= 0; i<mLayoutContainer.getChildCount(); i++){
            View view = mLayoutContainer.getChildAt(i);
            CheckBox chkBox = view.findViewById(R.id.chk_value);
            if (chkBox.isChecked()) {
                int id = (int)chkBox.getTag();
                if (mIds.equals("")){
                    mIds = String.valueOf(id);
                }else{
                    mIds += ("," + String.valueOf(id));
                }
            }
        }
        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_SUBMIT);
                request.addProperty("ConstituencyName", mActivity.mPlanConstituencyName);
                request.addProperty("PolingStationId", mActivity.mPlanPolingStationId);
                request.addProperty("AMFFacility", mIds);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_SUBMIT, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("UpdateAMFFacilityStatusResult")) {
                        final String result =  so.getPropertyAsString("UpdateAMFFacilityStatusResult");

                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.hideProgressDialog();
                                if (result != null && !result.equals("") && !result.equals("0")){

                                    Toast.makeText(mActivity, "Updated successfully.", Toast.LENGTH_SHORT).show();


                                }else {
                                    Toast.makeText(mActivity, "Update failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        return;

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mActivity.mPos = -1;
    }
}
