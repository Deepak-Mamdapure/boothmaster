package com.boothmaster;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.boothmaster.model.PolingStationData;
import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.widget.TableRow.LayoutParams;

public class UpdateVotingStatusFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    TextView mTxtChoose3;
    TextView mTxtChoose4;
    TextView mTxtLastTime;
    int constituencyId = -1;
    int polingStationId = -1;

    EditText mEditValue1;
    EditText mEditValue2;
    EditText mEditValue3;

    TextView mTxtVote1;
    TextView mTxtVote2;
    TextView mTxtVote3;
    private TextView textViewBoothName;

    String PolingStationName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_update_poling_progress,
                container, false);
        mActivity = (MainActivity) getActivity();

        initControl();

        textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar");

        if (mActivity.mRoleId == 1) {
            getConstituencyName();
            getPolingStationName();
            textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar >  " + mTxtChoose3.getText().toString().trim() + " > " + mTxtChoose4.getText().toString().trim());
            setUIVisibility(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE);

        } else if (mActivity.mRoleId == 2 || mActivity.mRoleId == 3 || mActivity.mRoleId == 4) {

            getConstituencyName();
            textViewBoothName.setText("S24-Uttar Pradesh > Siddharth Nagar > " + mTxtChoose3.getText().toString().trim());
            loadPolingStation();
            setUIVisibility(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.VISIBLE);

        } else {
            setUIVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);

        }
        getPolingStationName();

        loadData();
        return mRootView;
    }

    private void getConstituencyName() {
        try {
            SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
            String constituencyName = pref.getString("ConstituencyName", "");
            constituencyId = Integer.parseInt(pref.getString("ConstituencyId", "")) ;
            mTxtChoose3.setText(constituencyName);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void getPolingStationName() {
        try {

            SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
            PolingStationName = pref.getString("PolingStationName", "");
            polingStationId = Integer.parseInt(pref.getString("PolingStationId", ""));
            mTxtChoose4.setText(PolingStationName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUIVisibility(int allText, int State, int area, int Constituency, int booth) {
        textViewBoothName.setVisibility(allText);
        mRootView.findViewById(R.id.layout_choose3).setVisibility(Constituency);
        mRootView.findViewById(R.id.layout_choose4).setVisibility(booth);
    }

    private void initControl() {

        textViewBoothName = (TextView) mRootView.findViewById(R.id.textViewBoothName);


        mEditValue1 = mRootView.findViewById(R.id.edit_value1);
        mEditValue2 = mRootView.findViewById(R.id.edit_value2);
        mEditValue3 = mRootView.findViewById(R.id.edit_value3);

        mTxtVote1 = mRootView.findViewById(R.id.txt_vote1);
        mTxtVote2 = mRootView.findViewById(R.id.txt_vote2);
        mTxtVote3 = mRootView.findViewById(R.id.txt_vote3);

        mTxtLastTime = mRootView.findViewById(R.id.txt_time);
        mTxtChoose3 = mRootView.findViewById(R.id.txt_choose3);
        mTxtChoose4 = mRootView.findViewById(R.id.txt_choose4);


        setCurrentStatusUIBasedOnRole();
        mRootView.findViewById(R.id.layout_choose3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < GlobalConst.SEARCH_PARAMS3[0].length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, GlobalConst.SEARCH_PARAMS3[0][i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        int selPos = menuItem.getItemId() - 1;
                        if (selPos != constituencyId) {
                            polingStationId = -1;
                            mTxtChoose4.setText(R.string.choose_booth);
                        }
                        constituencyId = selPos;
                        mTxtChoose3.setText(menuItem.getTitle());
                        loadPolingStation();
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        mRootView.findViewById(R.id.layout_choose4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity.mRoleId != 2 && mActivity.mRoleId != 3 && mActivity.mRoleId != 4) {
                    if (constituencyId < 0) {
                        return;
                    }
                }
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < mListPoliing.size(); i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, mListPoliing.get(i).polingStationId, Menu.NONE, mListPoliing.get(i).polingStationName);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        polingStationId = menuItem.getItemId();
                        mTxtChoose4.setText(menuItem.getTitle());
                        loadData();
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        mRootView.findViewById(R.id.layout_last_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] times = new String[]{
                        "6 am", "7 am", "8 am", "9 am", "10 am", "11 am", "12 pm",
                        "1 pm", "2 pm", "3 pm", "4 pm", "5 pm", "6 pm", "7 pm", "8 pm"
                };
                PopupMenu dropDownMenu = new PopupMenu(mActivity, v);

                for (int i = 0; i < times.length; i++) {
                    dropDownMenu.getMenu().add(Menu.NONE, i + 1, Menu.NONE, times[i]);
                }

                dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        mTxtLastTime.setText(menuItem.getTitle());
                        return true;
                    }
                });
                dropDownMenu.show();
            }
        });
        mRootView.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
    }


    private void setCurrentStatusUIBasedOnRole() {

        if (mActivity.mRoleId == 1 || mActivity.mRoleId == 2 || mActivity.mRoleId == 3) {
            mRootView.findViewById(R.id.layout_current_status).setVisibility(View.VISIBLE);
        } else {
            mRootView.findViewById(R.id.layout_current_status).setVisibility(View.GONE);
        }


    }

    List<PolingStationData> mListPoliing = new ArrayList<>();
    private final String METHOD_NAME_LOAD_POLING = "LoadPolingStationForUser";
    private final String SOAP_ACTION_LOAD_POLING = GlobalConst.NAMESPACE + "LoadPolingStationForUser";

    private void loadPolingStation() {
        final String name = mTxtChoose3.getText().toString();
        SharedPreferences pref = getActivity().getSharedPreferences(GlobalConst.PREF_NAME, 0);
        final String customerId = pref.getString("id", "");

        mListPoliing.clear();
        if (!mActivity.pDialog.isShowing()) {
            mActivity.showProgressDialog();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_POLING);
                request.addProperty("ConstituencyName", name);
                request.addProperty("CustomerId", customerId);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_POLING, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadPolingStationForUserResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadPolingStationForUserResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("MyData");

                                if (complex.hasProperty("PolingStationId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        PolingStationData data = new PolingStationData();
                                        try {
                                            data.polingStationId = Integer.parseInt(GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationId"));
                                            data.polingStationName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationName");
                                            mListPoliing.add(data);
                                        } catch (Exception e) {

                                        }
                                    }

                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mActivity.hideProgressDialog();

                                        }
                                    });
                                    return;
                                }
                            } catch (Exception e) {

                            }


                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }


    int mTotalVoter = 0;
    int mTotalMale = 0;
    int mTotalFemale = 0;
    private final String METHOD_NAME = "LoadPolingStationVoter";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "LoadPolingStationVoter";

    private void loadData() {
        mTotalVoter = 0;
        mTotalMale = 0;
        mTotalFemale = 0;
        mTxtVote1.setText("");
        mTxtVote2.setText("");
        mTxtVote3.setText("");
        if (!mActivity.pDialog.isShowing()) {
            mActivity.showProgressDialog();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("PolingStationId", polingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("LoadPolingStationVoterResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadPolingStationVoterResult");


                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                                if (complex.hasProperty("TotalVoter")) {


                                    try {
                                        final String totalVoter = GlobalConst.getPropertyAsName(complex, "TotalVoter");
                                        final String totalMale = GlobalConst.getPropertyAsName(complex, "MaleVoter");
                                        final String totalFeMale = GlobalConst.getPropertyAsName(complex, "FemaleVoter");

                                        try {
                                            mTotalVoter = Integer.parseInt(totalVoter);
                                        } catch (Exception e) {

                                        }
                                        try {
                                            mTotalMale = Integer.parseInt(totalMale);
                                        } catch (Exception e) {

                                        }
                                        try {
                                            mTotalFemale = Integer.parseInt(totalFeMale);
                                        } catch (Exception e) {

                                        }
                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                LoadCurrentPolingStatus();
                                                mActivity.hideProgressDialog();
                                                mTxtVote1.setText(totalVoter);
                                                mTxtVote2.setText(totalMale);
                                                mTxtVote3.setText(totalFeMale);
                                            }
                                        });
                                        return;
                                    } catch (Exception e) {

                                    }


                                }
                            } catch (Exception e) {

                            }
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mActivity.hideProgressDialog();
                                    Toast.makeText(mActivity, R.string.server_fail, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }


    private final String METHOD_NAME_SUBMIT = "UpdatePolingStatus";
    private final String SOAP_ACTION_SUBMIT = GlobalConst.NAMESPACE + "UpdatePolingStatus";

    private void submit() {


        String value1 = mEditValue1.getText().toString();
        String value2 = mEditValue2.getText().toString();
        String value3 = mEditValue3.getText().toString();
        final String value4 = mTxtLastTime.getText().toString();
        int val1 = 0;
        int val2 = 0;
        int val3 = 0;
        try {
            val1 = Integer.parseInt(value1);
        } catch (Exception e) {

        }
        try {
            val2 = Integer.parseInt(value2);
        } catch (Exception e) {

        }
        try {
            val3 = Integer.parseInt(value3);
        } catch (Exception e) {

        }

        if (value1.equals("") || value2.equals("") || value3.equals("") || value4.equals("")) {
            Toast.makeText(mActivity, "Please fill all fields.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (val1 > mTotalVoter || val2 > mTotalMale || val3 > mTotalFemale) {
            Toast.makeText(mActivity, "Please input smaller value than Voter count", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!mActivity.pDialog.isShowing()) {
            mActivity.showProgressDialog();
        }
        final int finalVal1 = val1;
        final int finalVal2 = val2;
        final int finalVal3 = val3;
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_SUBMIT);
                request.addProperty("ConstituencyName", mTxtChoose3.getText().toString().trim());
                request.addProperty("PolingStationId", polingStationId);
                request.addProperty("TotalVoted", finalVal1);
                request.addProperty("MaleVoted", finalVal2);
                request.addProperty("FeMaleVoted", finalVal3);
                request.addProperty("TimeUpdated", value4);
                request.addProperty("CustomerId", mActivity.mCustomerId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_SUBMIT, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;


                    if (so.hasProperty("UpdatePolingStatusResult")) {
                        final String result = so.getPropertyAsString("UpdatePolingStatusResult");

                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.hideProgressDialog();
                                if (result != null && !result.equals("") && !result.equals("0")) {

                                    Toast.makeText(mActivity, "Updated successfully.", Toast.LENGTH_SHORT).show();
                                    mActivity.onBackPressed();


                                } else {
                                    Toast.makeText(mActivity, "Update failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        return;

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private final String METHOD_NAME_LOAD_CURRENT_POLING_STATUS = "LoadCurrentPolingStatus";
    private final String SOAP_ACTION_LOAD_CURRENT_POLING_STATUS = GlobalConst.NAMESPACE + "LoadCurrentPolingStatus";

    private void LoadCurrentPolingStatus() {

        if (!mActivity.pDialog.isShowing()) {
            mActivity.showProgressDialog();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_CURRENT_POLING_STATUS);
                request.addProperty("PolingStationId", polingStationId);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_CURRENT_POLING_STATUS, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("LoadCurrentPolingStatus", so.toString());


                    if (so.hasProperty("LoadCurrentPolingStatusResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("LoadCurrentPolingStatusResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");


                                final List<HashMap<String, String>> mListPoliing = new ArrayList<>();


                                if (complex.hasProperty("TotalVoted")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        HashMap<String, String> data = new HashMap<>();
                                        try {
                                            data.put("TotalVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "TotalVoted"));
                                            data.put("MaleVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "MaleVoted"));
                                            data.put("FemaleVoted", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "FemaleVoted"));
                                            data.put("TimeUpdated", GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "TimeUpdated"));
                                            mListPoliing.add(data);
                                        } catch (Exception e) {

                                        }
                                    }
                                    Log.d("List_Size", String.valueOf(mListPoliing.size()));

                                    mActivity.hideProgressDialog();

                                    if (mListPoliing.size() > 0) {
                                        mActivity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                init(mListPoliing);
                                            }
                                        });

                                    }


                                }
                            } catch (Exception e) {

                            }
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }
            }
        }).start();
    }

    /**
     * This function add the headers to the table
     **/
    public void addHeaders() {

        TableLayout tl = mRootView.findViewById(R.id.table_main);
        TableRow tr = new TableRow(getActivity());
        tl.removeAllViews();
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Total Voted", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Male", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Female", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tr.addView(getTextView(0, "Time", Color.WHITE, Typeface.BOLD, ContextCompat.getColor(getActivity(), R.color.colorPrimary)));
        tl.addView(tr, getTblLayoutParams());
    }

    /**
     * This function add the data to the table
     *
     * @param mListPoliing
     */
    public void addData(List<HashMap<String, String>> mListPoliing) {
        TableLayout tl = mRootView.findViewById(R.id.table_main);

        for (int i = 0; i < mListPoliing.size(); i++) {

            TableRow tr = new TableRow(getActivity());
            tr.setLayoutParams(getLayoutParams());
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("TotalVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("MaleVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("FemaleVoted"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tr.addView(getTextView(i + 1, mListPoliing.get(i).get("TimeUpdated"), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(getActivity(), R.color.table_row_background)));
            tl.addView(tr, getTblLayoutParams());
        }
    }

    public void init(List<HashMap<String, String>> mListPoliing) {
        addHeaders();
        addData(mListPoliing);

    }

    private TextView getTextView(int id, String title, int color, int typeface, int bgColor) {
        TextView tv = new TextView(getActivity());
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(color);
        tv.setPadding(10, 10, 10, 10);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }

    @NonNull
    private LayoutParams getLayoutParams() {
        LayoutParams params = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }

    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
    }

}

