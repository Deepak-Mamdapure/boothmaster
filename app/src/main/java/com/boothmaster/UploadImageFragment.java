package com.boothmaster;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.boothmaster.utils.GlobalConst;

import com.squareup.picasso.Picasso;

import org.jibble.simpleftp.SimpleFTP;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class UploadImageFragment extends Fragment {
    View mRootView;
    MainActivity mActivity;
    ImageView mImgChk1;
    ImageView mImgChk2;
    ImageView mImgChk3;
    ImageView mImgThumb;
    ImageView mImgFrame;
    String mServerFileName = "";
    String mSaveFilePath = "";
    public static final int PICK_CAMERA = 3;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frag_upload_image,
                container, false);
        mActivity = (MainActivity)getActivity();


        initControl();
        selectFrame(0);
        return mRootView;
    }
    String mCamreaImage;
    private void initControl(){
        mImgChk1 = mRootView.findViewById(R.id.img_chk_1);
        mImgChk2 = mRootView.findViewById(R.id.img_chk_2);
        mImgChk3 = mRootView.findViewById(R.id.img_chk_3);
        mImgThumb = mRootView.findViewById(R.id.img_thumb);
        mImgFrame = mRootView.findViewById(R.id.img_frame);
        mRootView.findViewById(R.id.layout_chk1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFrame(0);
            }
        });
        mRootView.findViewById(R.id.layout_chk2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFrame(1);
            }
        });
        mRootView.findViewById(R.id.layout_chk3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFrame(2);
            }
        });
        mRootView.findViewById(R.id.img_take_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(Environment.getExternalStorageDirectory(),  "temp.jpg");

                mCamreaImage = photo.getAbsolutePath();
                Uri uri = FileProvider.getUriForFile(mActivity, mActivity.getApplicationContext().getPackageName() + ".provider", photo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, PICK_CAMERA);
            }
        });
        mRootView.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });
        SeekBar seekBar = mRootView.findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float zoom = (progress  + 10.0f) / 10.f;
                mImgThumb.setScaleX(zoom);
                mImgThumb.setScaleY(zoom);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void captureImage(){
        RelativeLayout layoutImage = mRootView.findViewById(R.id.layout_image);
        layoutImage.buildDrawingCache();
        Bitmap bitmap = layoutImage.getDrawingCache();
        File photo = new File(Environment.getExternalStorageDirectory(),  "save.jpg");
        mSaveFilePath = photo.getAbsolutePath();
        try {
            FileOutputStream out = new FileOutputStream(photo);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20,  out);

            long length = photo.length() / 1024; // Size in KB
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        layoutImage.destroyDrawingCache();
    }

    private void submit(){
        mActivity.showProgressDialog();
        captureImage();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    SimpleFTP ftp = new SimpleFTP();

                    // Connect to an FTP server on port 21.
                    ftp.connect("booth.psweb.in", 21, "BoothPhotoFTP", "Zbg2?o32");

                    // Set binary mode.
                    ftp.bin();

                    // Change to a new working directory on the FTP server.
//                    ftp.cwd("web");
                    mServerFileName = mActivity.mCustomerId  + System.currentTimeMillis() + ".jpg";


                    // You can also upload from an InputStream, e.g.
                    ftp.stor(new FileInputStream(new File(mSaveFilePath)), mServerFileName);

                    // Quit from the FTP server.
                    ftp.disconnect();
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mActivity.hideProgressDialog();
                            updateVotePhoto();
                        }
                    });
                    return;
                }catch (IOException e){
                    e.printStackTrace();
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, "Upload failed.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();

    }


    private final String METHOD_NAME_LOAD_POLING = "UpdateVoterPhoto";
    private final String SOAP_ACTION_LOAD_POLING = GlobalConst.NAMESPACE + "UpdateVoterPhoto";
    private void updateVotePhoto() {

        mActivity.showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME_LOAD_POLING);
                request.addProperty("PhotoName", mServerFileName);
                request.addProperty("CustomerId", mActivity.mCustomerId);


                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION_LOAD_POLING, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("UpdateVoterPhotoResult", so.toString());

                    if (so.hasProperty("UpdateVoterPhotoResult")) {
                        final String result =  so.getPropertyAsString("UpdateVoterPhotoResult");

                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.hideProgressDialog();
                                if (result != null && !result.equals("") && !result.equals("0")){

                                    Toast.makeText(mActivity, "Uploaded successfully.", Toast.LENGTH_SHORT).show();
                                    Uri contentUri = FileProvider.getUriForFile(mActivity, mActivity.getApplicationContext().getPackageName()+ ".provider", new File(mSaveFilePath));
                                    if (contentUri != null) {
                                        Intent shareIntent = new Intent();
                                        shareIntent.setAction(Intent.ACTION_SEND);
                                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                                        shareIntent.setDataAndType(contentUri, mActivity.getContentResolver().getType(contentUri));
                                        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                                        shareIntent.setType("image/png");
                                        startActivity(Intent.createChooser(shareIntent, "Choose an app"));


                                        File target = new File(Environment.getExternalStorageDirectory(),  "temp.jpg");
                                        if (target.exists() && target.isFile() && target.canWrite()) {
                                            target.delete();
                                            Log.d("d_file", "" + target.getName());
                                        }
                                    }

                                }else {
                                    Toast.makeText(mActivity, "Upload failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        return;

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.hideProgressDialog();
                        Toast.makeText(mActivity, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private void selectFrame(int pos){
        if (pos == 0){
            mImgChk1.setImageResource(R.drawable.ic_check_on);
            mImgChk2.setImageResource(R.drawable.ic_check_off);
            mImgChk3.setImageResource(R.drawable.ic_check_off);
            mImgFrame.setImageResource(R.drawable.selfie_frame);
        }else if (pos == 1){
            mImgChk1.setImageResource(R.drawable.ic_check_off);
            mImgChk2.setImageResource(R.drawable.ic_check_on);
            mImgChk3.setImageResource(R.drawable.ic_check_off);
            mImgFrame.setImageResource(R.drawable.frame2_new);
        }else if (pos == 2){
            mImgChk1.setImageResource(R.drawable.ic_check_off);
            mImgChk2.setImageResource(R.drawable.ic_check_off);
            mImgChk3.setImageResource(R.drawable.ic_check_on);
            mImgFrame.setImageResource(R.drawable.frame3);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == PICK_CAMERA && resultCode == mActivity.RESULT_OK){
            Picasso.with(mActivity).load("file://" + mCamreaImage).into(mImgThumb);
        }
    }
}
