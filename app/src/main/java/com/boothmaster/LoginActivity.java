package com.boothmaster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.boothmaster.utils.GlobalConst;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        setContentView(R.layout.activity_login);

        setTabImage();
        findViewById(R.id.layout_get_started).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        findViewById(R.id.btn_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        SharedPreferences pref = getSharedPreferences(GlobalConst.PREF_NAME, 0);
        String customerId = pref.getString("id", "");
        if (customerId != null && !customerId.equals("")) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

    }

    private final String METHOD_NAME = "ValidateUser";
    private final String SOAP_ACTION = GlobalConst.NAMESPACE + "ValidateUser";

    private void signIn() {
        EditText editUserName = findViewById(R.id.edit_mobile);
        EditText editPassword = findViewById(R.id.edit_password);
        final String mobile = editUserName.getText().toString();
        final String password = editPassword.getText().toString();
        if (mobile.equals("") || password.equals("")) {
            Toast.makeText(this, "Please fill all fields.", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                SoapObject request = new SoapObject(GlobalConst.NAMESPACE, METHOD_NAME);
                request.addProperty("UserName", mobile);
                request.addProperty("Password", password);

                SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                soapEnvelope.bodyOut = request;
                soapEnvelope.dotNet = true;
                soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
                soapEnvelope.setOutputSoapObject(request);

                HttpTransportSE transport = new HttpTransportSE(GlobalConst.FULL_URL);
                transport.debug = true;
                transport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

                try {
                    transport.call(SOAP_ACTION, soapEnvelope);


                    SoapObject so = (SoapObject) soapEnvelope.bodyIn;

                    Log.d("Login_Result", "response=" + so.toString());

                    if (so.hasProperty("ValidateUserResult")) {
                        SoapObject so1 = (SoapObject) so.getProperty("ValidateUserResult");

                        if (so1.hasProperty("diffgram")) {
                            try {
                                SoapObject soDiffg = (SoapObject) so1.getProperty("diffgram");
                                SoapObject soElemt = (SoapObject) soDiffg.getProperty("NewDataSet");
                                SoapObject complex = (SoapObject) soElemt.getProperty("Table");

                                if (complex.hasProperty("CustomerId")) {

                                    for (int i = 0; i < soElemt.getPropertyCount(); i++) {
                                        try {
                                            final String customerId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "CustomerId");
                                            final String customerName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "CustomerName");
                                            String mobile = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Mobile");
                                            String email = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "Email");
                                            final String roldId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "RoleId");

                                            final String ConstituencyId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "ConstituencyId");
                                            final String PolingStationId = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationId");
                                            final String ConstituencyName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "ConstituencyName");
                                            final String PolingStationName = GlobalConst.getPropertyAsName((SoapObject) soElemt.getProperty(i), "PolingStationName");


                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    hideProgressDialog();
                                                    try {
                                                        SharedPreferences pref = getSharedPreferences(GlobalConst.PREF_NAME, 0);
                                                        SharedPreferences.Editor editor = pref.edit();
                                                        editor.putString("id", customerId);
                                                        editor.putString("customerName", customerName);
                                                        editor.putInt("roleId", Integer.parseInt(roldId));

                                                        editor.putString("ConstituencyId", ConstituencyId);
                                                        editor.putString("PolingStationId", PolingStationId);
                                                        editor.putString("ConstituencyName", ConstituencyName);
                                                        editor.putString("PolingStationName", PolingStationName);
                                                        editor.commit();
                                                    } catch (Exception e) {

                                                    }
                                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                            return;
                                        } catch (Exception e) {

                                        }
                                    }


                                }
                            } catch (Exception e) {

                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
                                    Toast.makeText(LoginActivity.this, R.string.server_fail, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;

                        }

                    }

                } catch (XmlPullParserException e) {

                } catch (Exception e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Toast.makeText(LoginActivity.this, R.string.no_internet_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }


}
